package br.com.equipe10.csg.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncrytedPasswordUtils {

    public String encrytePassword(String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

}
