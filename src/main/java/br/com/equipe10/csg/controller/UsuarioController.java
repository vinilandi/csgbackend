package br.com.equipe10.csg.controller;

import br.com.equipe10.csg.dao.UsuarioDAO;
import br.com.equipe10.csg.entity.Usuario;
import br.com.equipe10.csg.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private UsuarioService usuarioService;

//  Cadastrar uma informação
    @PostMapping("/usuarios")
    private ResponseEntity<Usuario> save(@RequestBody Usuario usuario){
        Usuario result = usuarioService.save(usuario);
        return ResponseEntity.ok(result);
    }

//   Atualizar
    @PutMapping("/usuarios")
    private ResponseEntity<Usuario> update(@RequestBody Usuario usuarioNew) {
        return ResponseEntity.ok(usuarioService.update(usuarioNew));
    }

//   Retornar informações
    @GetMapping("/usuarios")
    private List<Usuario> getAll(){
        return (List<Usuario>) usuarioDAO.findAll();
    }

//   Retornar informação especifica
    @GetMapping("/usuarios/{id}")
    private ResponseEntity<Usuario> get(@PathVariable("id") Long id){
        return ResponseEntity.of(usuarioDAO.findById(id));
    }

//   Deletar informação do banco
    @DeleteMapping("/usuarios/{id}")
    private void delete(@PathVariable("id") Long id) {
        usuarioDAO.deleteById(id);
    }

}
