package br.com.equipe10.csg.controller;

import br.com.equipe10.csg.dao.EntidadeDAO;
import br.com.equipe10.csg.entity.Entidade;
import br.com.equipe10.csg.entity.Usuario;
import br.com.equipe10.csg.service.EntidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
public class EntidadeController {

    @Autowired
    private EntidadeDAO entidadeDAO;

    @Autowired
    private EntidadeService entidadeService;

    @PostMapping("/entidades")
    public ResponseEntity<Entidade> save(@RequestBody Entidade entidade){
        Entidade result = entidadeDAO.save(entidade);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/entidades")
    private ResponseEntity<Entidade> update(@RequestBody Entidade entidadeNew) {
        return ResponseEntity.ok(entidadeService.update(entidadeNew));
    }

    @GetMapping("/entidades")
    public List<Entidade> getAll(){
        return (List<Entidade>) entidadeDAO.findAll();
    }

    @GetMapping("/entidades/{id}")
    public ResponseEntity<Entidade> get(@PathVariable("id") Long id){
        return ResponseEntity.of(entidadeDAO.findById(id));
    }

    @DeleteMapping("/entidades/{id}")
    private void delete(@PathVariable("id") Long id) {
        entidadeDAO.deleteById(id);
    }
}
