package br.com.equipe10.csg.controller;

import br.com.equipe10.csg.dao.PermissaoDAO;
import br.com.equipe10.csg.entity.Permissao;
import br.com.equipe10.csg.service.PermissaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class PermissaoController {

    @Autowired
    private PermissaoDAO permissaoDAO;

    @Autowired
    private PermissaoService permissaoService;

    @PostMapping("/permissoes")
    public ResponseEntity<Permissao> save(@RequestBody Permissao permissao){
        Permissao result = permissaoDAO.save(permissao);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/permissoes")
    private ResponseEntity<Permissao> update(@RequestBody Permissao permissaoNew) {
        return ResponseEntity.ok(permissaoService.update(permissaoNew));
    }

    @GetMapping("/permissoes")
    public List<Permissao> getAll(){
        return (List<Permissao>) permissaoDAO.findAll();
    }

    @GetMapping("/permissoes/{id}")
    public ResponseEntity<Permissao> get(@PathVariable("id") Long id){
        return ResponseEntity.of(permissaoDAO.findById(id));
    }

    @DeleteMapping("/permissoes/{id}")
    private void delete(@PathVariable("id") Long id) {
        permissaoDAO.deleteById(id);
    }
}
