package br.com.equipe10.csg.controller;

import br.com.equipe10.csg.dao.DemandaDAO;
import br.com.equipe10.csg.entity.Demanda;
import br.com.equipe10.csg.entity.Demanda;
import br.com.equipe10.csg.service.DemandaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class DemandaController {

    @Autowired
    private DemandaDAO demandaDAO;

    @Autowired
    private DemandaService demandaService;

    @PostMapping("/demandas")
    public ResponseEntity<Demanda> save(@RequestBody Demanda demanda){
        Demanda result = demandaDAO.save(demanda);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/demandas")
    private ResponseEntity<Demanda> update(@RequestBody Demanda demandaNew) {
        return ResponseEntity.ok(demandaService.update(demandaNew));
    }

    @GetMapping("/demandas")
    public List<Demanda> getAll(){
        return (List<Demanda>) demandaDAO.findAll();
    }

    @GetMapping("/demandas/{id}")
    public ResponseEntity<Demanda> get(@PathVariable("id") Long id){
        return ResponseEntity.of(demandaDAO.findById(id));
    }

    @DeleteMapping("/demandas/{id}")
    private void delete(@PathVariable("id") Long id) {
        demandaDAO.deleteById(id);
    }
}
