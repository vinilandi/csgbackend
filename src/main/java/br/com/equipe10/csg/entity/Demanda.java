package br.com.equipe10.csg.entity;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "Demanda")
public class Demanda {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "usuario", nullable = false)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name="entidade")
    private Entidade entidade;

    @Column(name = "criacao", nullable = false)
    private LocalDate criacao;

    @Column(name = "conclusao", nullable = false)
    private LocalDate conclusao;

    @Column(name = "prioridade", nullable = false)
    private Prioridade prioridade;

    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    public void update(Demanda demanda) {
        this.usuario = demanda.getUsuario();
        this.entidade = demanda.getEntidade();
        this.criacao = demanda.getCriacao();
        this.conclusao = demanda.getConclusao();
        this.prioridade = demanda.getPrioridade();
        this.status = demanda.getStatus();
        this.descricao = demanda.getDescricao();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Entidade getEntidade() {
        return entidade;
    }

    public void setEntidade(Entidade entidade) {
        this.entidade = entidade;
    }

    public LocalDate getCriacao() {
        return criacao;
    }

    public void setCriacao(LocalDate criacao) {
        this.criacao = criacao;
    }

    public LocalDate getConclusao() {
        return conclusao;
    }

    public void setConclusao(LocalDate conclusao) {
        this.conclusao = conclusao;
    }

    public Prioridade getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Prioridade prioridade) {
        this.prioridade = prioridade;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
