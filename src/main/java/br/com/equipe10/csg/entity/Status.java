package br.com.equipe10.csg.entity;

public enum Status {

    NAO_INICIADO,
    EM_ANDAMENTO,
    CANCELADO,
    CONCLUIDO

}
