package br.com.equipe10.csg.entity;

import javax.persistence.*;

@Entity
@Table(name = "Permissao")
public class Permissao {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    public void update(Permissao permissao) {
        this.name = permissao.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
