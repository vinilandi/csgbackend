package br.com.equipe10.csg.entity;

public enum Prioridade {

    CRITICA,
    ALTA,
    MEDIA,
    BAIXA

}
