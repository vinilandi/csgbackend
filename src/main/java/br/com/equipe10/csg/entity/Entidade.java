package br.com.equipe10.csg.entity;

import javax.persistence.*;

@Entity
@Table(name = "Entidade")
public class Entidade {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "descricao", nullable = false)
    private String descricao;


    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void update(Entidade entidade) {
        this.descricao = entidade.getDescricao();
    }

}
