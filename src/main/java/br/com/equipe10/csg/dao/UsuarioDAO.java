package br.com.equipe10.csg.dao;

import br.com.equipe10.csg.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UsuarioDAO extends CrudRepository<Usuario, Long> {

    @Query(value = "SELECT * FROM Usuario WHERE NOME = ?1", nativeQuery = true)
    Usuario findByNome(String nome);

    @Query(value = "select * from Usuario order by nome desc ", nativeQuery = true)
    List<Usuario> getUsuariosOrdAlf();

}
