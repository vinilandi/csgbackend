package br.com.equipe10.csg.dao;

import br.com.equipe10.csg.entity.Permissao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface PermissaoDAO extends CrudRepository<Permissao, Long> {

}
