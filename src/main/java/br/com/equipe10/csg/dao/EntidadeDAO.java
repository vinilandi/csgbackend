package br.com.equipe10.csg.dao;

import br.com.equipe10.csg.entity.Entidade;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface EntidadeDAO extends CrudRepository<Entidade, Long> {

    @Query(value = "select e.* from entidade e " +
            "inner join demanda d on d.entidade = e.id ", nativeQuery = true)
    List<Entidade> getEntidadesComDemanda();

    @Query(value = "select * from entidade e " +
            "order by descricao desc ", nativeQuery = true)
    List<Entidade> getEntidadesOrdAlf();

}
