package br.com.equipe10.csg.dao;

import br.com.equipe10.csg.entity.Demanda;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface DemandaDAO extends CrudRepository<Demanda, Long> {

    @Query(value = "select d.* from demanda d " +
            "inner join usuario u on u.id = d.usuario " +
            "where u.id = ?1 ", nativeQuery = true)
    List<Demanda> getDemandasUsuario(Long id);

    @Query(value = "select d.* from demanda d " +
            "inner join entidade e on e.id = d.entidade " +
            "where e.id = ?1 ", nativeQuery = true)
    List<Demanda> getDemandasEntidade(Long id);
}
