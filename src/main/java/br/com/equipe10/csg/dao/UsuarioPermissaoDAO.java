package br.com.equipe10.csg.dao;

import br.com.equipe10.csg.entity.Usuario;
import br.com.equipe10.csg.entity.UsuarioPermissao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UsuarioPermissaoDAO extends JpaRepository<UsuarioPermissao, Long> {

    @Query(value = "select u.* from usuario u " +
            "inner join usuario_permissao up on up.usuario = u.id " +
            "inner join permissao p on p.id = up.permissao " +
            "where p.name = ?1", nativeQuery = true)
    List<Usuario> getUsuariosDescrPerfil(String descricaoPerfil);

    @Query(value = "SELECT p.name FROM permissao p " +
            "INNER JOIN usuario_permissao up ON up.id = p.id " +
            "WHERE up.id = ?1 ", nativeQuery = true)
    List<String> getPermissoesUsuario(Long userId);
}
