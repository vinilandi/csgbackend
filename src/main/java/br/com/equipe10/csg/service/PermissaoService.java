package br.com.equipe10.csg.service;

import br.com.equipe10.csg.dao.PermissaoDAO;
import br.com.equipe10.csg.entity.Permissao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PermissaoService {

    @Autowired
    private PermissaoDAO permissaoDAO;

    public Permissao update(Permissao permissao) {
        Optional<Permissao> newPermissao = permissaoDAO.findById(permissao.getId());
        if (!newPermissao.isPresent()) {
//            throw new Exception("Usuário inexistente");
        }

        Permissao perm = newPermissao.get();
        perm.update(permissao);
        perm = permissaoDAO.save(perm);

        return perm;
    }
}
