package br.com.equipe10.csg.service;

import br.com.equipe10.csg.dao.DemandaDAO;
import br.com.equipe10.csg.entity.Demanda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DemandaService {

    @Autowired
    private DemandaDAO demandaDAO;

    public Demanda update(Demanda demanda) {
        Optional<Demanda> newDemanda = demandaDAO.findById(demanda.getId());
        if (!newDemanda.isPresent()) {
//            throw new Exception("Usuário inexistente");
        }

        Demanda dem = newDemanda.get();
        dem.update(demanda);
        dem = demandaDAO.save(dem);

        return dem;
    }
}
