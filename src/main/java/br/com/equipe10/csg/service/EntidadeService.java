package br.com.equipe10.csg.service;

import br.com.equipe10.csg.dao.EntidadeDAO;
import br.com.equipe10.csg.entity.Entidade;
import br.com.equipe10.csg.entity.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EntidadeService {

    @Autowired
    private EntidadeDAO entidadeDAO;

    public Entidade update(Entidade entidade) {
        Optional<Entidade> newEntidade = entidadeDAO.findById(entidade.getId());
        if (!newEntidade.isPresent()) {
//            throw new Exception("Usuário inexistente");
        }

        Entidade ent = newEntidade.get();
        ent.update(entidade);
        ent = entidadeDAO.save(ent);

        return ent;
    }
}
