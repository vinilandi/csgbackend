package br.com.equipe10.csg.service;

import br.com.equipe10.csg.dao.PermissaoDAO;
import br.com.equipe10.csg.dao.UsuarioDAO;
import br.com.equipe10.csg.dao.UsuarioPermissaoDAO;
import br.com.equipe10.csg.entity.Usuario;
import br.com.equipe10.csg.utils.EncrytedPasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private UsuarioPermissaoDAO usuarioPermissaoDAO;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Usuario user = usuarioDAO.findByNome(userName);

        if (user == null)
            throw new UsernameNotFoundException("User " + userName + " was not found in the database");

        List<String> roleNames = usuarioPermissaoDAO.getPermissoesUsuario(user.getId());

        List<GrantedAuthority> grantList = new ArrayList<>();
        if (roleNames != null) {
            for (String role : roleNames) {
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }

        UserDetails userDetails = (UserDetails) new User(user.getNome(), //
                user.getSenha(), grantList);

        return userDetails;
    }

    public Usuario save(Usuario usuario) {
        usuario.setSenha(new EncrytedPasswordUtils().encrytePassword(usuario.getSenha()));
        usuario = usuarioDAO.save(usuario);
        return usuario;
    }

    public Usuario update(Usuario usuario) {
        Optional<Usuario> newUsuario = usuarioDAO.findById(usuario.getId());
        if (!newUsuario.isPresent()) {
//            throw new Exception("Usuário inexistente");
        }

        Usuario user = newUsuario.get();
        user.update(usuario);
        user = usuarioDAO.save(user);

        return user;
    }

}
